import 'package:flutter_blue/flutter_blue.dart';

class ENRGBeaconScaner{
  int _master_id = 0;
  FlutterBlue flutterBlue;
  void Function(int master_id, int excurs, int track, int timestamp) _callback;

  /**
   *  __Конструктор класса__.
   *
   * [master_id] - код привязки ENRG.
   *
   * Если [master_id] == 0, то будут приниматься метки от всех передатчиков (необходимо для сканирования имеющихся в эфире передатчиков)
   *
   * Если [master_id] != 0, то будут приниматься метки только от заданного передатчика.
   */
  ENRGBeaconScaner({int master_id = 0}){
    _master_id = master_id;
    flutterBlue = FlutterBlue.instance;
  }

  /**
   * __Запуск сканирования__
   *
   * [callback] - функция типа void Function(int master_id, int excurs, int track, int timestamp),
   * вызываемая при получении метки.
   *
   * [master_id] - код привязки передатчика, от которого получена метка;
   *
   * [excurs] - код экскурсии;
   *
   * [track] - № трека;
   *
   * [timestamp] - время от начала трека в мс.
   */
  Future scan(void Function(int master_id, int excurs, int track, int timestamp) callback) async {
    print("START SCAN");

    _callback = callback;

    await flutterBlue.stopScan();

    flutterBlue.startScan();//timeout: new Duration(seconds: 1)

    var subscription = flutterBlue.scanResults.listen((results) {
      for (ScanResult r in results) {
        if(_master_id==0){
          for(int key in r.advertisementData.manufacturerData.keys){
            if((key & 0x8000) != 0) {
              int m_id = key & 0x7FFF;
              _processData(m_id, r.advertisementData.manufacturerData[key]);
            }
          }
        }else{
          if(r.advertisementData.manufacturerData.containsKey(0x8000 + _master_id)) {
            List<int> data = r.advertisementData.manufacturerData[0x8000 + _master_id];
            _processData(_master_id, data);
          }
        }
      }
    });
  }

  void _processData(int m_id, List<int> data){
    if(data[0] == 0xA5){
      int excurs = data[1];
      int track = (data[2] << 8) + data[3];
      int timestamp = (data[4] << 24) + (data[5] << 16) + (data[6] << 8) + (data[7] << 0);

      //print('$excurs/$track - $timestamp');

      _callback(m_id, excurs, track, timestamp);
    }
  }

  /**
   * __Остановка сканирования__
   */
  void stop(){
    flutterBlue.stopScan();
  }

  /**
   * __Установка listener-а для получения информации о состоянии сканирования__
   *
   * [callback] - функция типа void Function(bool value)
   *
   * [value] - состояние сканирования.
   *
   */
  void setIsScaningListener(void Function(bool value) callback){
    flutterBlue.isScanning.listen(callback);
  }
}