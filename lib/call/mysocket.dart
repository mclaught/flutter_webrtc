import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:fluttertestapp/utils/websocket.dart';
import 'package:web_socket_channel/io.dart';

typedef void OnMessageCallback(dynamic msg);
typedef void OnCloseCallback(int code, String reason);
typedef void OnOpenCallback();

abstract class MyBaseSocket{
  String _host;
  int _port;
  OnOpenCallback onOpen;
  OnMessageCallback onMessage;
  OnCloseCallback onClose;

  void connect();
  void close();
  void send(String data);

  void parseUrl(url){
    Uri uri = Uri.parse(url);
    _host = uri.host;
    _port = uri.port;
    print("${_host}:${_port}");
  }
}

class MyAsyncSocket extends MyBaseSocket{
  Socket _sock;
  String buf = "";

  MyAsyncSocket(url){
    parseUrl(url);
    print("SOCKET: $_host:$_port");
  }

  void connect() async{
    _sock = await Socket.connect(_host, _port);
    _sock.listen(dataHandler, onError: errorHandler, onDone: doneHandler, cancelOnError: false);
    if(onOpen != null)
      onOpen();
  }

  void dataHandler(Uint8List data){
    for(int i=0; i<data.length; i++){
      int c = data[i];
      if(c != 0) {
        buf += String.fromCharCode(c);
      }else{
        if(onMessage != null){
          onMessage(buf);
          buf = "";
        }
      }
    }

//    String str = String.fromCharCodes(data);
//    List<String> parts = str.split("\n");
//    for(String part in parts){
//      print("RECV: $part");
//      if(onMessage != null && part.length > 1){
//        onMessage(part);
//      }
//    }
  }

  void doneHandler(){
    if(_sock != null) {
      _sock.close().then((socket){
        _sock = null;
      });
    }
    if(onClose != null){
      onClose(0, "");
    }
  }

  void errorHandler(error, StackTrace trace){
    print("SOCKET ERROR: $error");
  }

  void close(){
    if(_sock != null) {
      _sock.close();
      print("SOCKET CLOSED");
    }
  }

  void send(String data){
    if(_sock != null) {
      List<int> buf = utf8.encode(data+String.fromCharCode(0));
      _sock.add(buf);
    }
  }
}

class MyWebSocket extends MyBaseSocket{
  IOWebSocketChannel _sock;
  //SimpleWebSocket _sock;
  String url;

  MyWebSocket(url){
    this.url = url;
    parseUrl(url);
  }

  @override
  void connect()async {
    _sock = IOWebSocketChannel.connect(url);
    _sock.stream.listen(onMessage, onDone: doneHandler, onError: errorHandler);//

    if(onOpen != null)
      onOpen();
//    Future.delayed(Duration(milliseconds: 5000), (){
//    });

//    _sock = SimpleWebSocket(url);
//    _sock.onMessage = onMessage;
//    _sock.onClose = onClose;
//    _sock.onOpen = onOpen;
//    _sock.connect();
  }

  @override
  void close() {
    if(_sock != null && _sock.sink != null)
      _sock.sink.close();
  }

  @override
  void send(String data) {
    if(_sock!=null && _sock.sink != null)
      _sock.sink.add(data);
  }

  void dataHandler(event) {
    onMessage(event);
  }

  void doneHandler() {
    if(_sock != null && _sock.sink != null) {
      _sock.sink.close();
      _sock = null;
    }
    if(onClose != null){
      onClose(0, "");
    }
  }

  void errorHandler(error, stacktrace){
    print(error);
  }
}

class MyWebSocket2 extends MyBaseSocket{
  SimpleWebSocket _sock;
  String url;


  MyWebSocket2(this.url);

  @override
  void connect() {
    _sock = SimpleWebSocket(url);
    _sock.onOpen = onOpen;
    _sock.onClose = onClose;
    _sock.onMessage = onMessage;
    _sock.connect();
  }

  @override
  void close() {
    if(_sock!=null)
      _sock.close();
  }

  @override
  void send(String data) {
    if(_sock!=null)
        _sock.send(data);
  }

}