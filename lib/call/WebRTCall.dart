import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
//import 'call/signaling.dart';
import 'mysignaling.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';

import 'db_controller.dart';

class WebRTCall {

  final String _dbSession;
  final String _dbURL;
  int _userId;
  final String _webrtcServer;
  void Function(SignalingState state, String text) onState;
  final RTCVideoRenderer _localRenderer = new RTCVideoRenderer();
  final RTCVideoRenderer _remoteRenderer = new RTCVideoRenderer();
  Signaling _signaling;
  DBController _dbController;
  int _callId = -1;
  Timer _timer = null;
  int excurs = -1;

  var _enable_audio = true;
  var _enable_video = true;

  /**
   * __Конструктор__
   *
   * [_dbSession] ID сессии
   *
   * [_dbURL] Адрес сервера БД
   *
   * [_webrtcServer] Адрес сервера WebRTC
   *
   * [onState] Callback функция типа void Function(SignalingState state), вызываемая при смене состояния вызова
   */
  WebRTCall(this._dbSession, this._dbURL, this._webrtcServer, {bool audio=true, bool video=false}){
    _enable_audio = audio;
    _enable_video = video;
    _dbController = DBController(_dbURL, dbSession: _dbSession);
    _initRenderers();
  }

  _initRenderers() async {
    await _localRenderer.initialize();
    await _remoteRenderer.initialize();
  }

  RTCVideoRenderer get localRenderer => _localRenderer;
  RTCVideoRenderer get remoteRenderer => _remoteRenderer;

  /**
   * __Деактивация класса__
   *
   */
  destroy(){
    hangUp();
    deleteCall();
    _localRenderer.dispose();
    _remoteRenderer.dispose();
  }

  /**
   * __Соединение__
   *
   * [excurs] ID экскурсии для создания заявки на сервере БД. Для гидов этот параметр устанавливается = -1
   */
  Future connect({int excurs = -1, int callId = -1})async{
    dynamic user = await _dbController.sessionUser();
    _userId = int.parse(user['id']);

    if (_signaling == null){
      _signaling = new Signaling(_webrtcServer, _userId.toString(), audio: _enable_audio, video: _enable_video);

      _signaling.onStateChange = (SignalingState state) {
        print("STATE = $state");

        switch (state) {
          case SignalingState.CallStateNew:
            setState(state, "Новый вызов");
            break;
          case SignalingState.CallStateBye:
            if(_localRenderer != null)
              _localRenderer.srcObject = null;
            if(_remoteRenderer != null)
              _remoteRenderer.srcObject = null;
            setState(state, "Вызов звершен");
            break;
          case SignalingState.CallStateInvite:
            setState(state, "Приглашение");
            break;
          case SignalingState.CallStateConnected:
            setState(state, "Вызов создан");
            break;
          case SignalingState.CallStateRinging:
            setState(state, "Сигнал");
            break;
          case SignalingState.ConnectionClosed:
            deleteCall();
            setState(state, "Соединение разорвано");
            break;
          case SignalingState.ConnectionError:
            setState(state, "Ошибка соединения");
            break;
          case SignalingState.ConnectionOpen:
            setState(state, "Соединение установлено");
            if(excurs != -1){
              createCall(excurs).then((callId){
                _callId = callId;
                print(callId);
                setState(SignalingState.CallStateID, "Создана заявка №$callId");

              }).catchError((error){
                print(error);
              });
            }else if(callId != -1){
              addToCall(callId).then((callId){
                _callId = callId;
                setState(SignalingState.CallStateID, "Поключен к беседе №$callId");
              }).catchError((error){
                print(error);
              });
            }
            break;
          case SignalingState.CallStateID:
            break;
        }
      };

      _signaling.onPeersUpdate = ((event) {
//        this.setState(() {
//          _selfId = event['self'];
//          _peers = event['peers'];
//        });
      });

      _signaling.onLocalStream = ((stream) {
        _localRenderer.srcObject = stream;
        _signaling.switchCamera();
      });

      _signaling.onAddRemoteStream = ((stream) {
        _remoteRenderer.srcObject = stream;
      });

      _signaling.onRemoveRemoteStream = ((stream) {
        _remoteRenderer.srcObject = null;
      });

      _signaling.connect();

//      _signaling.enableSpeakerPhone(false);
    }
  }

  /**
   * __Вызов пользователя__
   *
   * Осуществляется гидом
   */
  invite(int peerId) async {
    if (_signaling != null && peerId != _userId) {
      _signaling.invite(peerId.toString(), 'video', false);
    }
  }

  /**
   * __Завершение вызова__
   */
  void hangUp() {
    if (_signaling != null) {
      _signaling.bye();
      _signaling.close();
      _signaling = null;
    }
  }

  /**
   * __Создание заявки в БД__
   *
   * Осуществляется пользователем(слушателем) с указанием ID экскурсии.
   *
   * Вызывать этот метод явно в нормальном цикле работы не требуется. Заявка удаляется автоматически при разрыве соединения.
   *
   * [excurs] ID экскурсии
   *
   * Возвращает ID вызова(заявки)
   */
  Future<int> createCall(int excurs)async{
    _callId = await _dbController.newCall(excurs, _userId.toString(), _webrtcServer);
    _timer = Timer.periodic(Duration(minutes: 1), (timer){
      if(_callId != -1)
        _dbController.touchCall(_callId).catchError((error){
          print("TOUCH ERROR: $error");
        });
    });
    return _callId;
  }

  /**
   * __Удаление заявки__
   *
   * Вызывать этот метод явно в нормальном цикле работы не требуется. Удаление заявки произойдет автоматически, при завершении вызова.
   */
  void deleteCall()async{
    if(_callId != -1){
      await _dbController.deleteCall(_callId);
      if(_timer!=null)
        await _timer.cancel();
      _timer = null;
      _callId = -1;
    }
  }

  /**
   * __Добавить пользователя__
   */
  Future<int> addToCall(int callId)async{
    _callId = await _dbController.addToCall(callId);
    _timer = Timer.periodic(Duration(minutes: 1), (timer){
      if(_callId != -1)
        _dbController.touchCall(_callId).catchError((error){
          print("TOUCH ERROR: $error");
        });
    });
    return callId;
  }

  void setState(SignalingState state, String str) {
    if(onState != null)
      onState(state, str);
  }

  bool get video_enabled{
    if(_signaling == null)
      return false;
    return _signaling.video_enabled;
  }

  set video_enabled(bool value){
    if(_signaling != null) {
      _signaling.video_enabled = value;
//      if(value){
//        _signaling.switchCamera();
//      }
    }
  }

  void switchCamera(){
    if(_signaling != null){
      _signaling.switchCamera();
    }
  }
}