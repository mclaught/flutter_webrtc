import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';

class DBController {
  final String dbServer;
  String dbSession;

  DBController(this.dbServer, {this.dbSession = ""});

  dynamic _request(String url, Map<String, String> body)async{
    //print("Request: ${body}");
    Response resp = await post(url, body: body);
    if(resp.statusCode >= 300){
      throw Exception("HTTP error ${resp.statusCode}: ${resp.reasonPhrase}");
    }
    //print("Response: ${resp.body}");
    dynamic json = jsonDecode(resp.body);
    if(json['result'] != "success"){
      throw Exception(json['error']);
    }
    return json;
  }

  Future<String> singin(String user, String pass)async{
    dynamic json = await _request("$dbServer/signin.php", {"user":user, "pass":pass});
    if(json['result'] != 'success')
      throw Exception(json['error']);
    dbSession = json['session'];
    return dbSession;
  }

  Future<dynamic> sessionUser() async {
    dynamic json = await _request("$dbServer/call.php", {"session":dbSession, "action":"session_user"});
//    print("JSON: ${json}");
    return json["user"];
  }

  Future<int> newCall(int excurs, String callSession, String callServer) async{
    dynamic json = await _request("$dbServer/call.php", {'action':'new_call', 'session':dbSession, 'excurs':"$excurs", 'data': callSession, 'server': callServer});
    return json['call_id'];
  }

  void deleteCall(int callId)async{
    await _request("$dbServer/call.php", {'action':'delete_call', 'session':dbSession, 'call':"$callId"});
  }

  Future touchCall(int callId) async {
    await _request("$dbServer/call.php", {'action':'touch_call', 'session':dbSession, 'call':"$callId"});
  }

  Future<dynamic> guideCalls()async{
    dynamic json = await _request("$dbServer/call.php", {'action':'guide_calls', 'session':dbSession});
    if(json['result'] != 'success'){
      throw Exception(json['error']);
    }

    return json['calls'];
  }

  Future<dynamic> callUsers(int callId)async{
    dynamic json = await _request("$dbServer/call.php", {'action':'call_users', 'session':dbSession, 'call_id':'$callId'});
    if(json['result'] != 'success'){
      throw Exception(json['error']);
    }
    return json['users'];
  }

  addToCall(int callId)async {
    dynamic json = await _request("$dbServer/call.php", {'action':'add_call_user', 'session':dbSession, 'call':"$callId"});
    return callId;
  }
}