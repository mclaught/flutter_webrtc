import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
//import 'call/signaling.dart';
import 'call/mysignaling.dart';
import 'call/WebRTCall.dart';
import 'call/db_controller.dart';

class CallWidget extends StatefulWidget{
  final String title;
  final String dbSession;
  final String dbURL;
  final String signURL;
  final int excurs;
  final int callId;

  const CallWidget({Key key, this.title, this.dbURL, this.signURL, this.dbSession, this.excurs=-1, this.callId=-1}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CallWidgetState(dbURL, signURL, dbSession, excurs, callId);

}

class _CallWidgetState extends State<CallWidget> {
  int _userId;
  final String dbURL;
  final String signURL;
  final String dbSession;
  final int excurs;
  final int callId;
  List<dynamic> _peers;
  var _selfId;
  final _formKey = GlobalKey<FormState>();
//  var _myIdController = TextEditingController();
//  var _peerIdController = TextEditingController();
  final String _server = "192.168.1.1";
  DBController _dbController;
  String _status = "Disconnected";
  WebRTCall _webRTCall;
  Timer timer = null;

  _CallWidgetState(this.dbURL, this.signURL, this.dbSession, this.excurs, this.callId);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Пользователь"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: (){
                if(_webRTCall != null)
                  _webRTCall.hangUp();
              }
          ),
          IconButton(
            icon: Icon(Icons.video_call),
            onPressed: (){
              _webRTCall.video_enabled = !_webRTCall.video_enabled;
            }
          )
        ],
      ),
      body: Center(
//        child: Column(
//          children: <Widget>[
//            Form(
//              key: _formKey,
//              child: Column(
//                children: <Widget>[
//                  Text("Мой ID"),
//                  TextFormField(
//                    controller: _myIdController,
//                  ),
//                  Text("Пригласить"),
//                  TextFormField(
//                    controller: _peerIdController,
//                  )
//                ],
//              ),
//            ),
//          ],
//        ),
      ),
      bottomSheet: Container(
        padding: EdgeInsets.all(8.0),
        width: MediaQuery.of(context).size.width,
        color: Colors.lightBlue,
        child: Text(_status, style: TextStyle(color: Colors.white),),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initSession();
  }

  void initSession() async {
    try{
      _dbController = DBController(dbURL, dbSession: dbSession);

      dynamic user = await _dbController.sessionUser();//
      print(user);

      _userId = int.parse(user['id']);
      print("USER_ID = $_userId");
//      setState(() {
//        _myIdController.text = "$_userId";
//      });
    }catch(e){
      print("ERROR: $e");
    }

    //ВАЖНО:
    _webRTCall = WebRTCall(dbSession, dbURL, signURL, video: false);
    _webRTCall.onState = (SignalingState state, String text){
      setStatus(text);
    };
    _webRTCall.connect(excurs: excurs, callId: callId);//Указываем ID экскурсии только для пользователя (создается запись в БД)
  }

  @override
  deactivate() {
    super.deactivate();

    if(_webRTCall != null)
      _webRTCall.destroy();

    if(timer != null) {
      timer.cancel();
      timer = null;
    }
  }

  void setStatus(String status){
    try {
      setState(() {
        _status = status;
      });
    }catch(e){

    }
  }

}