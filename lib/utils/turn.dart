import 'dart:convert';
import 'dart:async';
import 'dart:io';

Future<Map> getTurnCredential(String host, int port) async {
    HttpClient client = HttpClient(context: SecurityContext());
    client.badCertificateCallback =
        (X509Certificate cert, String host, int port) {
      print('getTurnCredential: Allow self-signed certificate => $host:$port. ');
      return true;
    };
//  HttpClient client = HttpClient();

  //{username: 1590428012:flutter-webrtc, password: 9ITEA/iFoYiIOIPhdlYwO7rDbQM, ttl: 86400, uris: [turn:192.168.1.1:19302?transport=udp]}

    var url = 'https://$host:$port/api/turn?service=turn&username=flutter-webrtc';
    var request = await client.getUrl(Uri.parse(url));
    var response = await request.close();
    var responseBody = await response.transform(Utf8Decoder()).join();
    print('getTurnCredential:response => $responseBody.');
    Map data = JsonDecoder().convert(responseBody);
    return data;
  }
