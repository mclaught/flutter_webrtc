import 'dart:async';

import 'package:flutter/material.dart';
import 'call/WebRTCall.dart';
//import 'call/signaling.dart';
import 'call/mysignaling.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';

import 'call/db_controller.dart';

class GuideCallWidget extends StatefulWidget{
  final String dbSession;
  final String dbURL;
  final String signURL;

  const GuideCallWidget({Key key, this.dbSession, this.dbURL, this.signURL}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _GuideCallWidgetSate(dbSession, dbURL, signURL);

}

class _GuideCallWidgetSate extends State<GuideCallWidget> {
  final String dbSession;
  final String dbURL;
  final String signURL;
  List<Widget> _items = [Text("Empty"), Text("Empty")];
  DBController _dbController;
  dynamic _calls;
  WebRTCall _webRTCall;
  String _status = "Not connected";
  int _callId;
  int _invite_id = -1;

  _GuideCallWidgetSate(this.dbSession, this.dbURL, this.signURL);

  @override
  void initState() {
    _dbController = DBController(dbURL, dbSession: dbSession);
    _update();
    Timer.periodic(Duration(seconds: 1), (timer){
      _update();
    });

    //ВАЖНО:
    _webRTCall = WebRTCall(dbSession, dbURL, signURL);
    _webRTCall.onState = (SignalingState state, String text){
      setStatus(text);
    };
    _webRTCall.connect();

  }


  @override
  void deactivate() {
    if(_webRTCall != null)
      _webRTCall.destroy();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Guide ($_status)"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.update),
            onPressed: (){
              _update();
            },
          )
        ],
      ),
      body: Center(
        child: ListView.builder(itemBuilder: (context, index){
          if(_calls == null)
            return null;
          if(index >= _calls.length)
            return null;
          return _createItem(_calls[index]);
        }),
      ),
      bottomSheet: Container(
        padding: EdgeInsets.all(8.0),
        width: MediaQuery.of(context).size.width,
        color: Colors.lightBlue,
        child: Text(_status, style: TextStyle(color: Colors.white),),
      ),
    );
  }

  void _update()async{
    _dbController.guideCalls().then((calls){//ВАЖНО: Список заявок для данного гида (гид идентифицируется по ID сессии)
      //print(calls);

      if(mounted){
        setState(() {
          _calls = calls;
        });
      }else{
        //print("NOT MOUNTED");
      }
    }).catchError((error){
      print("ERROR: $error");
    });
  }

  Widget _createItem(call) {
    final _imgSz = 128.0;
    final _cardMargins = 16.0;
    final _imgMargins = 16.0;
    final _textMargins = 16.0;
    final _textWidth = MediaQuery.of(context).size.width - _imgSz - _cardMargins*2 - _textMargins*2 - _imgMargins;

    dynamic excurs = call['excurs'];
    //print("CREATE: $excurs");

    int call_id = int.parse(call['call_id']);

    return Card(
      margin: EdgeInsets.only(left: _cardMargins, top: _cardMargins, right: _cardMargins),
      elevation: 5.0,
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: _imgMargins, top: _imgMargins, bottom: _imgMargins),
            height: _imgSz,
            width: _imgSz,
            child: _invite_id==call_id ? RTCVideoView(_webRTCall.remoteRenderer) : Image.network("$dbURL/${excurs['preview']}",
            ),
          ),
          Column(
            children: <Widget>[
              Container(
                width: _textWidth,
                margin: EdgeInsets.all(_textMargins),
                child: Text(
                  excurs['name'],
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                  textAlign: TextAlign.left,
                ),
              ),
              Container(
                width: _textWidth,
                margin: EdgeInsets.all(_textMargins),
                child: Text(excurs['description'], overflow: TextOverflow.ellipsis, maxLines: 5,),
              ),
              Container(
                width: _textWidth,
                margin: EdgeInsets.all(_textMargins),
                child: FlatButton(
                  child: Text("Пригласить", style: TextStyle(color: Colors.white),),
                  color: Colors.lightBlue,

                  onPressed: (){
                    _dbController.callUsers(call_id).then((dynamic users){//ВАЖНО: Список пользователей в заявке (на несколько пользователей пока не тестировалось)
                      print("USERS: $users");

                      for(dynamic user in users){
                        int user_id = int.parse(user['id']);
                        _webRTCall.invite(user_id);//ВАЖНО: Вызов пользователя
                        setState(() {
                          _invite_id = call_id;
                        });
                      }
                    }).catchError((error){
                      print("CALL_USERS ERROR: %error");
                    });
                  },
                ),
              )


            ],
          )
        ],
      )
    );


  }

  void setStatus(String state) {
    if(mounted){
      setState(() {
        _status = state;
      });
    }
  }
}